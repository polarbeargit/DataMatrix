#ifndef DMTXENCODESCHEME_H_
#define DMTXENCODESCHEME_H_

/* Verify stream is using expected scheme */
#define CHKSCHEME(s) { \
   if(stream->currentScheme != (s)) { StreamMarkFatal(stream, DmtxErrorUnexpectedScheme); return; } \
}

/* CHKERR should follow any call that might alter stream status */
#define CHKERR { \
   if(stream->status != DmtxStatusEncoding) { return; } \
}

/* CHKSIZE should follows typical calls to FindSymbolSize()  */
#define CHKSIZE { \
   if(sizeIdx == DmtxUndefined) { StreamMarkInvalid(stream, DmtxErrorUnknown); return; } \
}

#endif /* DMTXENCODESCHEME_H_ */
