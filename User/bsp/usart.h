#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 	


void uart_init(u32 bound); //初始化IO 串口1
void USART1_SendByte(u8 c);   //串口1发送一个字节                	
void USART1_SendlStr(u8 *s);       //串口1发送字符串        	
void USART1_SendlStrLen(u8 *s,u32 len);              	


#endif


