#include "stm32f10x.h"
#include "usart.h"
#include "dmtx.h"

int dmtxEncode(unsigned char *str,int strlen,int sizeIdxRequest,
                    unsigned char *img,int* width,int* height)
{
    DmtxEncode enc;
    memset(&enc,0,sizeof(enc));
    
    enc.scheme = DmtxSchemeAscii;
    enc.sizeIdxRequest = sizeIdxRequest;
    enc.marginSize = 1;
    enc.moduleSize = 1;
    enc.pixelPacking = DmtxPack24bppRGB;
    enc.imageFlip = DmtxFlipNone;
    enc.rowPadBytes = 0;
    
    dmtxMatrix3Identity(enc.xfrm);
    if(dmtxEncodeDataMatrix(&enc, strlen, str)==0)
        return 0;
    
    *width = dmtxImageGetProp(&enc.image, DmtxPropWidth);
    *height = dmtxImageGetProp(&enc.image, DmtxPropHeight);

    unsigned char *pxl = enc.image.pxl;
    int i,j;
    for(i=0;i<*height;i++)
    {
        for (j = 0; j < *width; j++) 
        {
            if (pxl[i*(*width)*3+j*3]==0xff) 
            {
                img[i*(*width)+j] = 0xff;
            } 
            else 
            {
                img[i*(*width)+j] = 0x00;
            }
        }
    }
    return 1;
}

void dmtxImgPrintf(const unsigned char *img,int width,int height)
{
    int i,j;
    printf("width=%d,height=%d\r\n",width,height);
    for(i=0;i<height;i++)
    {
        for (j = 0; j < width; j++) 
        {
            if (img[i*width+j]==0xff) 
            {
                printf("██");
            } 
            else 
            {
                printf("  ");
            }
        }
        printf("\r\n");
    }
    printf("\r\n");
}

unsigned char img[2048];
int img_width;
int img_height;

int main(void)
{
	uart_init(115200);
    int i;
    for(i=DmtxSymbol10x10;i<=DmtxSymbol44x44;i++)
    {
        if(dmtxEncode((unsigned char*)"1234567890",10,i,img,&img_width,&img_height))
            dmtxImgPrintf(img,img_width,img_height);
        else
            printf("\r\n**********************error***************************\r\n");
    }
    
    for(i=DmtxSymbol8x18;i<=DmtxSymbol16x48;i++)
    {
        if(dmtxEncode((unsigned char*)"1234567890",10,i,img,&img_width,&img_height))
            dmtxImgPrintf(img,img_width,img_height);
        else
            printf("\r\n**********************error***************************\r\n");
    }

    while(1)
    {

    }
	
	return 0;
}
