
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f10x.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define KEY1_Pin GPIO_Pin_0
#define KEY1_GPIO_Port GPIOA
#define KEY1_EXTI_IRQn EXTI0_IRQn
#define SX1276_DIO0_Pin GPIO_Pin_1
#define SX1276_DIO0_GPIO_Port GPIOA
#define SX1276_DIO0_EXTI_IRQn EXTI1_IRQn
#define SX1276_DIO0_EXTI_LINE EXTI_Line1
#define SX1276_SPI_NSS_Pin GPIO_Pin_4
#define SX1276_SPI_NSS_GPIO_Port GPIOA
#define SX1276_SPI_SCK_Pin GPIO_Pin_5
#define SX1276_SPI_SCK_GPIO_Port GPIOA
#define SX1276_SPI_MISO_Pin GPIO_Pin_6
#define SX1276_SPI_MISO_GPIO_Port GPIOA
#define SX1276_SPI_MOSI_Pin GPIO_Pin_7
#define SX1276_SPI_MOSI_GPIO_Port GPIOA
#define SX1276_RESET_Pin GPIO_Pin_0
#define SX1276_RESET_GPIO_Port GPIOB
#define LED_GREEN_Pin GPIO_Pin_1
#define LED_GREEN_GPIO_Port GPIOB
#define SX1276_TXRX_Pin GPIO_Pin_2
#define SX1276_TXRX_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define LED_GREEN_ON()		 (LED_GREEN_GPIO_Port->BRR   = LED_GREEN_Pin)
#define LED_GREEN_OFF()		(LED_GREEN_GPIO_Port->BSRR  = LED_GREEN_Pin)
#define LED_GREEN_TOG()		(LED_GREEN_GPIO_Port->ODR  ^= LED_GREEN_Pin)
#define KEY1_READ()       GPIO_ReadInputDataBit(KEY1_GPIO_Port,KEY1_Pin)
/* USER CODE END Private defines */



#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
